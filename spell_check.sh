#!/bin/sh

EXITCODE=0
for lang in "en_US" "fr_BE"; do
	echo "$lang"
	make "$lang" > /dev/null 2>&1
	pdftotext CV_Emil_Vanherp_"$lang".pdf
	OUTPUT=$(hunspell -l -d "$lang" -p dict_"$lang".txt CV_Emil_Vanherp_"$lang".txt)
	if [ -n "${OUTPUT}" ]; then
		echo "Spelling errors:"
		echo "$OUTPUT"
		EXITCODE=1
	fi
done
exit "$EXITCODE" 
