english: ## English cv
	latexmk -jobname=CV_Emil_Vanherp_en_US -pdf -e '$$pdflatex=q/xelatex %O "\let\buildenglish\iftrue\input{%S}"/' cv.tex

en_US: english

french: ## French cv
	latexmk -jobname=CV_Emil_Vanherp_fr_BE -pdf -e '$$pdflatex=q/xelatex %O "\let\buildfrench\iftrue\input{%S}"/' cv.tex

fr_BE: french

dutch: ## Dutch cv
	latexmk -jobname=CV_Emil_Vanherp_en_BE -pdf -e '$$pdflatex=q/xelatex %O "\let\builddutch\iftrue\input{%S}"/' cv.tex

nl_BE: dutch

clean: ## clean all files
	rm -f *.pdf *.aux *.log *.xdv *.fls *.fdb_latexmk *.run.xml *.bak* *.bbl *.bcf *.blg *.synctex.gz

test: ## spellcheck etc
	./lint_test.sh
	textidote cv.tex
	./spell_check.sh

help:  # http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@grep -P '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
.PHONY: english en_US french fr_BE dutch nl_BE clean test help
